<?php declare(strict_types=1);


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Article
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="article")
 */
class Article {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $urlId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $urlSlug;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $headline;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $subtitle;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $introduction;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $displayDate;

    /**
     * @var Author
     * @ORM\OneToOne(targetEntity="Author", cascade={"persist"})
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @var Image
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
     */
    private $image;

    /**
     * @var Chapter[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Chapter", mappedBy="article", cascade={"persist", "remove"})
     */
    private $chapters;

    /**
     * Article constructor.
     */
    public function __construct() {
        $this->chapters = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUrlId(): int {
        return $this->urlId;
    }

    /**
     * @param int $urlId
     */
    public function setUrlId(int $urlId): void {
        $this->urlId = $urlId;
    }

    /**
     * @return string
     */
    public function getUrlSlug(): string {
        return $this->urlSlug;
    }

    /**
     * @param string $urlSlug
     */
    public function setUrlSlug(string $urlSlug): void {
        $this->urlSlug = $urlSlug;
    }

    /**
     * @return string
     */
    public function getHeadline(): string {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline(string $headline): void {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getIntroduction(): string {
        return $this->introduction;
    }

    /**
     * @param string $introduction
     */
    public function setIntroduction(string $introduction): void {
        $this->introduction = $introduction;
    }

    /**
     * @return \DateTime
     */
    public function getDisplayDate(): \DateTime {
        return $this->displayDate;
    }

    /**
     * @param array $dates
     */
    public function setDisplayDate(array $dates): void {

        foreach ($dates as $dateData) {
            $date = new \DateTime();
            $date->setTimestamp((int)$dateData);
            $this->displayDate = $date;
        }

    }

    /**
     * @return Author
     */
    public function getAuthor(): Author {
        return $this->author;
    }

    /**
     * @param Author $author
     */
    public function setAuthor(Author $author): void {
        $this->author = $author;
    }

    /**
     * @return Image
     */
    public function getImage(): Image {
        return $this->image;
    }

    /**
     * @param Image $image
     */
    public function setImage(Image $image): void {
        $this->image = $image;
    }

    /**
     * @return Collection
     */
    public function getChapters(): Collection {
        return $this->chapters;
    }

    /**
     * @param Chapter[] $chapters
     */
    public function setChapters(array $chapters): void {
        $this->chapters = $chapters;
    }

    /**
     * @param Chapter $chapter
     */
    public function addChapter(Chapter $chapter): void {
        if (!$this->chapters->contains($chapter)) {
            $this->chapters->add($chapter);
        }
    }

}
