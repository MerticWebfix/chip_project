<?php declare(strict_types=1);


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Image
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="image")
 */
class Image {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $height;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $width;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $text;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $url;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $source;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getHeight(): int {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getWidth(): int {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void {
        $this->width = $width;
    }

    /**
     * @return string
     */
    public function getText(): string {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getUrl(): string {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getSource(): string {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void {
        $this->source = $source;
    }
}
