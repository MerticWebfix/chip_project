<?php declare(strict_types=1);


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Chapter
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="chapter")
 */
class Chapter {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $orderInArticle;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $headline;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $text;

    /**
     * @var Image|null
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
     */
    private $image;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="chapters")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderInArticle(): int {
        return $this->orderInArticle;
    }

    /**
     * @param int $orderInArticle
     */
    public function setOrderInArticle(int $orderInArticle): void {
        $this->orderInArticle = $orderInArticle;
    }

    /**
     * @return string
     */
    public function getHeadline(): string {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline(string $headline): void {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getText(): string {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void {
        $this->text = $text;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image {
        return $this->image;
    }

    /**
     * @param Image|null $image
     */
    public function setImage(?Image $image): void {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getArticle() {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void {
        $this->article = $article;
    }

}
