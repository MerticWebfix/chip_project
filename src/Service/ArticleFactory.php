<?php declare(strict_types=1);


namespace App\Service;


use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Chapter;
use App\Entity\Image;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ArticleFactory {

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ArticleFactory constructor.
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     */
    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer) {
        $this->em = $em;
        $this->serializer = $serializer;
    }


    /**
     * @param  array $data
     * @return Article|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(array $data): ?Article {

        $article = new Article();
        $article->setId($data['id']);
        $article->setUrlId($data['urlId']);
        $article->setUrlSlug($data['urlSlug']);
        $article->setHeadline($data['headline']);
        $article->setSubtitle($data['subtitle']);
        $article->setIntroduction($data['introduction']);

        $image = $this->mapImage($data['image']);
        $article->setImage($image);

        $author = $this->mapAuthor($data['author']);
        $article->setAuthor($author);
        $article->setDisplayDate($data['displayDate']);

        foreach ($data['chapters'] as $chapterData) {
            $chapter = $this->mapChapter($chapterData);
            $chapter->setArticle($article);
            $article->addChapter($chapter);
        }

        $this->em->persist($article);

        $this->em->flush();

        return $article;
    }

    /**
     * @param array $authorData
     * @return Author
     */
    private function mapAuthor(array $authorData):Author {
        $author = new Author();

        $author->setId($authorData['id']);
        $author->setFirstName($authorData['firstName']);
        $author->setLastName($authorData['lastName']);

        return $author;
    }

    /**
     * @param $imageData
     * @return Image
     */
    private function mapImage($imageData): Image {

        $image = new Image();

        $image->setId($imageData['id']);
        $image->setHeight($imageData['height']);
        $image->setWidth($imageData['width']);
        $image->setUrl($imageData['url']);
        $image->setSource($imageData['source']);
        $image->setText($imageData['text']);

        return $image;
    }

    /**
     * @param $chapterData
     * @return Chapter
     */
    private function mapChapter(array $chapterData): Chapter {
        $chapter = new Chapter();

        $chapter->setId($chapterData['id']);
        $chapter->setOrderInArticle($chapterData['order']);
        $chapter->setText($chapterData['text']);
        $chapter->setHeadline($chapterData['headline']);
        if ($chapterData['image']) {
            $image = $this->mapImage($chapterData['image']);
            $chapter->setImage($image);
        }

        return $chapter;
    }
}
