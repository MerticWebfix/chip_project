<?php declare(strict_types=1);


namespace App\Controller;


use App\Form\ImportType;
use App\Service\ArticleFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ImportController
 * @package App\Controller
 */
class ImportController extends AbstractController{

    /**
     * @Route("/import", name="show_import_form", methods={"GET"})
     */
    public function showImportForm(Request $request): Response {

        $form = $this->createForm(ImportType::class);

        return $this->render('import.html.twig', [
            'form' => $form->createView(),
            'message' => $request->get('message')
        ]);
    }

    /**
     * @Route("import", name="process_import_form", methods={"POST"})
     * @param Request        $request
     * @param ArticleFactory $articleFactory
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function processImportForm(Request $request, ArticleFactory $articleFactory) {
        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $file */
            $file = $form['file']->getData();
            $content = file_get_contents($file->getPathname());

            $data = json_decode($content, true);

            if ($data) {
                try {
                    $article = $articleFactory->create($data);
                } catch (\Exception $e) {
                    unset($e);
                    $this->addFlash('error', 'Nepodařilo se uložit.');
                    return $this->redirectToRoute('show_import_form');
                }

                $this->addFlash('success','Soubor byl importován.');
                return $this->redirectToRoute('show_import_form');
            }

            $this->addFlash('error','Nevalidní json.');
            return $this->render('import.html.twig', [
                'form' => $form->createView()
            ]);

        }



        return $this->render('import.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
