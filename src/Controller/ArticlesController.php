<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticlesController
 * @package App\Controller
 */
class ArticlesController extends AbstractController
{
    /**
     * @Route("/", name="articles")
     */
    public function index(): Response {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repository->findBy([], ['displayDate' => 'DESC']);

        return $this->render('articles.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @param $url
     * @Route("/article/{url}", name="article_detail")
     * @return Response
     */
    public function detail($url) {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $article = $repository->findOneBy(['urlSlug' => $url]);

        return $this->render('detail.html.twig', [
            'article' => $article
        ]);
    }

}
