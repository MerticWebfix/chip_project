<?php declare(strict_types=1);


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ImportType
 * @package App\Form
 */
class ImportType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'file',
            FileType::class,
            [
                'label' => 'Soubor',
                'required' => false,
                'constraints' => [
                   new NotBlank()
                ]
            ]
        );

        $builder->add(
            'submit',
            SubmitType::class,
            [
                'label' => 'Importovat'
            ]
        );
    }
}
